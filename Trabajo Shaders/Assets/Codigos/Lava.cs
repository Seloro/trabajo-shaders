using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    void Update()
    {
        movimiento();
    }
    void movimiento()// mueve el objeto hacia arriba asta un cierto punto
    {
        if (transform.position.y < -.25)
        {
            transform.position += Vector3.up * Time.deltaTime;
        }
        else
        {
            transform.position = new Vector3(transform.position.x, -.25f, transform.position.z);
        }
    }
}
