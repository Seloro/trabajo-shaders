using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instanciador : MonoBehaviour
{
    public GameObject plataforma, burbuja, bolaLava;
    GameObject ayuda;
    float tempPlataforma = 1;
    float tempLava;
    bool lavaActivada;

    void Start()
    {
        for (int i = 6; i <= 24; i += 6)// intancia 4 plataformas cada 6 de distancia
        {
             ayuda = Instantiate(plataforma, Vector3.up * i + Vector3.right * UnityEngine.Random.Range(-12, 13), Quaternion.identity);
            Instantiate(burbuja, ayuda.transform);
        }
    }
    void Update()
    {
        crearplataformas();
        crearLava();
    }
    void crearplataformas()// instancia plataformascada 6 segundos
    {
        tempPlataforma += Time.deltaTime;
        if (tempPlataforma > 6)
        {
            ayuda = Instantiate(plataforma, transform.position + Vector3.right * UnityEngine.Random.Range(-12, 13), Quaternion.identity);
            Instantiate(burbuja, ayuda.transform);
            tempPlataforma = 0;
        }
    }

    void crearLava()
    {
        if (lavaActivada)
        {
            tempLava += Time.deltaTime;
            if (tempLava > 4)
            {
                Instantiate(bolaLava, Vector3.right * UnityEngine.Random.Range(-12, 13), Quaternion.identity);
                tempLava = 0;
            }
        }
        if (UI.puntos > 10)
            lavaActivada = true;
    }
}
