using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boladelava : MonoBehaviour
{
    Rigidbody rb;
    public float vMin, vMax;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        rb.AddForce(Vector3.up * Random.Range(vMin, vMax), ForceMode.Impulse);
    }

    void Update()
    {
        if (transform.position.y < -5)
        {
            Destroy(gameObject);
        }
    }
}
