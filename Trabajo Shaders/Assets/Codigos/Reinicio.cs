using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reinicio : MonoBehaviour
{
    public void reiniciar()
    {
        UI.fin = false;
        UI.puntos = 0;
        SceneManager.LoadScene(1);
    }

    public void salir()
    {
        UI.fin = false;
        UI.puntos = 0;
        SceneManager.LoadScene(0);
    }
}
