using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento_Horizontal : MonoBehaviour
{
    bool meMuevo;
    int signo;
    void Start()
    {
        if (UnityEngine.Random.Range(1, 11) > 7)// determina si se mueve o no y asigna un valor al signo
        {
            meMuevo = true;
            do
            {
                signo = UnityEngine.Random.Range(-1, 1);
            } while (signo == 0);// elije un signo al asar (1 � -1)
        }
    }
    void Update()
    {
        if (meMuevo)
        {
            movimiento();
            correccionDeSigno();
        }
    }
    void movimiento()// movimiento horizontal
    {
        transform.position += Vector3.right * signo * Time.deltaTime * 2;
    }
    void correccionDeSigno()// cambia el signo cundo se pasa de los limites fijados
    {
        if (transform.position.x <= -12 || transform.position.x >= 12)
        {
            signo *= -1;
        }
    }
}
