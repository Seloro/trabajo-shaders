using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menuinicio : MonoBehaviour
{
    public GameObject inicio, creditos, info;

    public void Cambio(string cambar)
    {
        if (cambar == "creditos")
        {
            creditos.SetActive(true);
            inicio.SetActive(false);
        }
        else if (cambar == "Info")
        {
            info.SetActive(true);
            inicio.SetActive(false);
        }
        else if (cambar == "Salir")
            Application.Quit();
        else
        {
            info.SetActive(false);
            creditos.SetActive(false);
            inicio.SetActive(true);
        }
    }

    public void jugar()
    {
        GLOBAL.LimiteParaGanar = 25;
        SceneManager.LoadScene(1);
    }
}
