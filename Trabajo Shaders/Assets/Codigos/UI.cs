using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public GameObject infoFinal;
    public TextMeshProUGUI textEstado, texPuntos;
    static public int puntos = 0;
    static public bool fin = false;
    void Start()
    {
        infoFinal.gameObject.SetActive(false);
    }
    void Update()
    {
        if (!fin)// muestra cuatos puntos tiene el jugador en partida
        {
            texPuntos.text = "Orbes: " + puntos;
        }
        else// indica que se termino lapartida, cambia la escala de tiempo a 0 y informa al jugador sus puntos y como reinicial la partida
        {
            Time.timeScale = 0;
            textEstado.text = "Perdiste";
            infoFinal.gameObject.SetActive(true);
        }

        if (GLOBAL.ganar)
        {
            Time.timeScale = 0;
            textEstado.text = "Ganaste";
            infoFinal.gameObject.SetActive(true);
        }
    }
}
