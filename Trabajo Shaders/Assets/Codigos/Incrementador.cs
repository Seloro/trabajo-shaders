using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Incrementador : MonoBehaviour
{
    float i = 1;
    void Update()// incrementa la escala del tiempo asta 2
    {
        if (i < 2)
        {
            i += Time.deltaTime / 100;
        }
        else
        {
            i = 2;
        }
        Time.timeScale = i;
    }
}
