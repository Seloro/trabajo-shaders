using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nuves_Texturas : MonoBehaviour
{
    int signo;
    public List<Texture2D> nuves;
    Material mat;
    public bool movimientoPermitido;
    void Start()
    {
        mat = GetComponent<MeshRenderer>().material;
        mat.SetTexture("_Textura_Base", nuves[UnityEngine.Random.Range(0, nuves.Count)]);// carga al shader SH_Nuves una textura de nuves al asar en la lista de texturas
        if (movimientoPermitido)
        {
            do
            {
                signo = UnityEngine.Random.Range(-1, 1);
            } while (signo == 0);// eleije un signo al asar (1 � -1)
        }
    }
    void Update()
    {
        if (movimientoPermitido)
        {
            movimiento();
        }
    }
    void movimiento()// muevela esca del objeto de forma constante entre los intervalos establecidos
    {
        if (transform.localScale.x <= 1.35f || transform.localScale.x >= 1.65f)
        {
            signo *= -1;
        }
        transform.localScale += (Vector3.right + Vector3.up) * signo * Time.deltaTime / 10;
    }
}
