using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sol : MonoBehaviour
{

    void Start()
    {
        transform.position += Vector3.right * Random.Range(-12, 13);
        GLOBAL.ganar = false;
    }

    void Update()
    {
        vajar();
    }

    void vajar()
    {
        if (UI.puntos >= GLOBAL.LimiteParaGanar)
        {
            if (transform.position.y > 18)
                transform.position -= Vector3.up * Time.deltaTime;
            else
                transform.position = new Vector3(transform.position.x, 18, transform.position.z);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
            GLOBAL.ganar = true;
    }
}
