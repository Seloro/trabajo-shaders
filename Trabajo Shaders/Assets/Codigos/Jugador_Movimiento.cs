using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Jugador_Movimiento : MonoBehaviour
{
    public Image indicador;
    public Color color1, color2;
    Rigidbody rb;
    public float velocidad, inpulso;
    bool inmortalidad;
    float tempSaltoEspecial;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        transform.position += Vector3.right * UnityEngine.Random.Range(-16, 17);
    }
    void Update()
    {
        movimiento();
        reinicioDeTemporizadores();
    }
    void movimiento()// mueve al jugador
    {
        if (Input.GetKey(KeyCode.D))
        {
            rb.velocity += Vector3.right * Time.deltaTime * velocidad;
        }
        else if (Input.GetKey(KeyCode.A))
        {
            rb.velocity -= Vector3.right * Time.deltaTime * velocidad;
        }
        if ((Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space)) && Physics.Raycast(transform.position, Vector3.down, 0.6f))
        {
            rb.AddForce(Vector3.up * inpulso, ForceMode.Impulse);
        }
        if (Input.GetKeyDown(KeyCode.Q))// hace inmune a la lava al jugador (tranpa)
        {
            inmortalidad = !inmortalidad;
        }
        if (UI.puntos >= 5 && tempSaltoEspecial <= 0)
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.velocity = Vector3.zero;
                rb.AddForce(Vector3.up * inpulso, ForceMode.Impulse);
                UI.puntos -= 5;
                tempSaltoEspecial = 5 * Time.timeScale;
            }
        }
    }
    private void OnCollisionEnter(Collision collision)// destrulle al juagdor cuando toca lava y termina la partida
    {
        if (collision.gameObject.CompareTag("Lava") && !inmortalidad)
        {
            UI.fin = true;
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)// destrulle la burbuja cuando la y aumenta puntos
    {
        if (other.gameObject.CompareTag("Puntos"))
        {
            Destroy(other.gameObject);
            UI.puntos++;
        }
    }

    void reinicioDeTemporizadores()
    {
        if (tempSaltoEspecial > 0)
        {
            tempSaltoEspecial -= Time.deltaTime;
            indicador.color = color2;
        }
        else
        {
            indicador.color = color1;
        }
    }
}
