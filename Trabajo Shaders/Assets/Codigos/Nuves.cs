using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UIElements;

public class Nuves : MonoBehaviour
{
    BoxCollider col;
    public LayerMask ignorarJugador, noignorarJugador;

    private void Start()
    {
        col = GetComponent<BoxCollider>();
    }

    void Update()
    {
        actualizarColiciones();
    }

    void actualizarColiciones()
    {
        if (GLOBAL.Jugador != null)
        {
            if (GLOBAL.Jugador.transform.position.y <= transform.position.y + .4f)
            {
                col.excludeLayers = ignorarJugador;
            }
            else
            {
                col.excludeLayers = noignorarJugador;
            }
        }
    }
}
