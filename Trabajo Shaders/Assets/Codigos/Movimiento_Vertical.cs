using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento_Vertical : MonoBehaviour
{
    void Update()
    {
        eliminacion();
        movimiento();
    }
    void eliminacion()// destrulle el objeto cuando se encuenta en una y menor a -10
    {
        if (transform.localPosition.y < -10)
        {
            Destroy(gameObject);
        }
    }
    void movimiento()// mueve el objeto hacia abajo
    {
        transform.position -= Vector3.up * Time.deltaTime;
    }
}
